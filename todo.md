# TODO

- [ ] Autoupdate FeatureCollection when uploading new file
- [ ] Use Postgis database
- [ ] Use npm instead of CDN to have everythin offline
- [ ] Outsource Layout switcher
- [ ] if distance between points too big, make them separate features (how to handle plane/train?)
- [ ] fix loss of geojson data when switching map style
- [ ] allign tracks to streets
    - [ ] check if mapmatching allows train tracks
    - [ ] separate points when moving along street and when staying inside building 
- [ ] assign colors of colormap according to date, not file index
- [ ] show colorbar to represent time
- [ ] Plot speed vs. time for some tracks to see how good separate walking/cycling/car/train
- [ ] add compass navigation button
