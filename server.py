# Credits to https://github.com/kazuar/flask_mapbox
import requests
from glob import glob
import geojson as gj
import seaborn as sns
from matplotlib.colors import rgb2hex
from flask import Flask, render_template
import gpxpy
import os

app = Flask(__name__)
MAPBOX_ACCESS_KEY = 'pk.eyJ1IjoiZHV6IiwiYSI6ImNsMzZjdW52MzFtdXUzYnA5a3c5Ym15MnYifQ.wOuZq5IAPKIxZm9eEE4TfA'
ROUTE_URL = "https://api.mapbox.com/matching/v5/mapbox/walking/{0}.json?access_token={1}&tidy=true&geometries=geojson"


# def create_route_url():
#     # Create a string with all the geo coordinates
#     k = sh.geometry.LineString(df[1].explode()[0][0].coords[0:10])
#     lat_longs = ";".join(
#         ["{0},{1}".format(point[0], point[1]) for point in k.coords])
#     # Create a url with the geo coordinates and access token
#     url = ROUTE_URL.format(lat_longs, MAPBOX_ACCESS_KEY)
#     return url


# def get_route_data():
#     # Get the route url
#     route_url = create_route_url()
#     # Perform a GET request to the route API
#     result = requests.get(route_url)
#     # Convert the return value to JSON
#     data = result.json()

#     geometry = data["matchings"][0]["geometry"]
#     route_data = Feature(geometry=geometry, properties={})
#     return route_data


features = []
files = sorted(glob('/home/daniel/Nextcloud/gps/*.gpx'))
colors = sns.color_palette('flare_r', len(files))
for file, color in zip(files, colors):
    with open(file) as f:
        gpx = gpxpy.parse(f)
    tracksegment = gpxpy.gpx.GPXTrackSegment(gpx.walk(only_points=True))
    # segment.simplify()
    # segment.smooth(horizontal=True, remove_extremes=True)
    # tracksegment.reduce_points(50)
    geometry = gj.LineString([(p.longitude, p.latitude, p.elevation)
                              for p in tracksegment.walk(only_points=True)])
    # print(geometry['coordinates'])
    feature = gj.Feature(geometry=geometry,
                         properties={"color": rgb2hex(color)})
    features.append(feature)
features = gj.FeatureCollection(features)


@ app.route('/')
def mapbox_gl():
    # route_data = get_route_data()
    return render_template('mapbox_gl.html',
                           ACCESS_KEY=MAPBOX_ACCESS_KEY,
                           route_data=features)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 5000)))
